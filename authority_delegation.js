
/**
 * Enables or disables form fields for a given role as appropriate.
 */
function authority_delegation_check_role(rid) {
  var prefix = 'edit-ad' + rid;
  var d = document.getElementById(prefix + '-delegatable');
  var l = document.getElementById(prefix + '-limit');
  var t = document.getElementById(prefix + '-time');
  var u = document.getElementById(prefix + '-unit');
  if ( d.checked ) {
    l.disabled = false;
    t.disabled = ! l.checked;
    u.disabled = ! l.checked;
  }
  else {
    l.disabled = true;
    t.disabled = true;
    u.disabled = true;
  }
}
