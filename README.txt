
README file for the Delegation of Authority Drupal module.


Description
***********

This module lets a user possessing a certain role delegate that role to
someone else for a limited amount of time.  It is intended to support
vacations and other absences wherein a user is not able or willing to
perform site-related duties.

Any role other than the "anonymous user" or "authenticated user" role
is potentially assignable.  The exact set is site-specific, and is configured
by the site administrator or someone possessing the 'administer site
configuration' permission.  The administrator has access to a settings page
that lets him control:

  * Which roles may be delegated.
  * Whether delegation is time-limited, and if so, the amount of time
    for which a delegation may be assigned.
  * The means available to select a user (see below).

If a user posseses a role that may be delegated, then he has a menu item that
takes him to a delegation page.  This page shows:

  * A form listing all his delegations currently in effect.  He may change
    any of these delegations.
  * A form in which he may enter a new delegation of authority.  He must
    select a user to whom authority is delegated, and a date on which
    delegation will end.  He may also enter a date on which delegation is to
    start.

User selection is implemented using hooks.  Hooks are implemented in files in
the "selectors" subdirectory.  This module provides a hook for selecting users
by Drupal user name, i.e., the value in the name column of the users table.
See the README.txt file in the selectors subdirectory for a description of how
to implement hooks.

In general, delegation of authority is activated and deactivated using cron.
This module therefore requires thata site run cron.

See also
********

RoleDelegation
RoleAssign (http://drupal.org/project/roleassign).


Installation
************

1. Extract the 'authority_delegation' module directory, including all its
   subdirectories, into your Drupal modules directory.

2. Go to the Administer > Site building > Modules page, and enable the module.

3. Go to the Administer > Site Configuration > Delegation of Authority page
   and specify which roles you want to be delegatable.

Credits
*******

* Developed by Steve Wartik (http://drupal.org/user/91820).
