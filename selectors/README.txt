

README file for authority_delegation/selectors.

This directory contains user selectors for the authority_delegation module.
A user selector is a set of hooks that provide the authority_delegation module
with:

  1) Information on the selector (hook INFO).

  2) A form that, when displayed, lets a user select an individual to whom
     authority should be delegated (hook FORM).

  3) A mapping from the value selected through hook FORM to a Drupal user ID
     (hook UID).

The intent is to let you provide sensible, intuitive mechanisms for selecting
users at your site.  This module comes with one user selector that lets a user
select someone based on Drupal login name.  You could also implement a selector
based on user profile information if your site uses that.

Each user selector is stored in a file in this directory.  The file's name must
end in ".inc".  The authority_delegation module expects each ".inc" file in
this directory to implement the user selector hooks.  Also, the portion of the
selector's name preceding ".inc" must consist only of alphanumeric characters.

Each hook is a function whose name consists of three parts, separated by
underscores:

  1) The prefix "authority_delegation_user_selector".

  3) The portion of the file name preceding ".inc".

  2) The hook name.

Here is a description of each hook.  See the file drupal.inc for an example.

hook INFO:

This hook returns information on the user selector in the file containing it.
It returns an associative array with two keys: name and description.  The value
of the "name" key is the human-readable form of the selector's name.  The value
of the "description" key is descriptive text.

hook FORM:

This hook returns a Drupal form that is incorporated into the page a user sees
when he follows the "Delegate your authority" menu item.  The nature of the
form returned by this hook depends entirely on how the user is to select
indivuduals to delegate.  It might be a pull-down menu or a text field, for
example.  It can define the '#validate' field to ensure that the selection made
is logical.

The hook is passed a single optional parameter, a Drupal UID.  The hook should
use this parameter to set the '#default_value' field of the form.

hook UID:

This hook takes as a parameter the form values that result from the user
filling in the form returned by hook FORM.  It translates this parameter into a
Drupal UID and returns that value.  The authority_delegation module uses this
returned value to effect changes in the users_roles table.

