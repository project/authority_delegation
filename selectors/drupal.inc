<?php
/**
 * Returns information on this selector.  The returned value is an array with:
 * @li One mandatory key, @c name, whose value will be displayed to the user.
 * @li One optional key, @c description, whose value if given will be used
 *     as to describe the selector.
 *
 * @return
 *   An array containing information on the Drupal user selector.
 */
function authority_delegation_user_selector_drupal_info() {
  return array(
		'name' => 'Drupal',
		'description' => t('Select users by Drupal login name.')
  );
}

/**
 * Returns a form that encapsulates the manner in which a user selects the
 * person to whom authority is delegated.  This selector returns a drop-down
 * box containing the names of all users in Drupal's {users} table who have
 * nonzero status.  (Also, this function filters out the current user, i.e.,
 * you can't delegate yourself.)  The value of each option is the user's
 * Drupal user ID.
 * @param $default_uid
 *   The value to use for the initially selected user.  If null, no user is
 *   initially selected.
 * @return
 *   A form that lets a person be selected by Drupal login name.
 */
function authority_delegation_user_selector_drupal_form($default_uid = null) {
	global $user;
	$qry = 'SELECT uid, name FROM {users} WHERE status != 0 AND uid != %d';
	$values = array(0 => '');
	$result = db_query($qry, $user->uid);
	while ( $u = db_fetch_object($result) ) {
		$values[$u->uid] = $u->name;
	}
	$selection = array(
		'#title' => t('User is'),
		'#type' => 'select',
		'#options' => $values,
		'#attributes' => array('class' => 'drupal-user-selector')
	);
	if ( $default_uid && array_key_exists($default_uid, $values) ) {
		$selection['#default_value'] = $default_uid;
	}

	return $selection;
}

/**
 * Returns the Drupal user ID corresponding to the form value array passed as
 * a parameter.  For this selector, that means just returning the parameter.
 * @param $form_values
 *   The value derived from the authority_delegation_user_selector_form hook.
 * @return
 *   The value from the select widget in the form hook.
 */
function authority_delegation_user_selector_drupal_uid($form_values) {
	return $form_values;
}
